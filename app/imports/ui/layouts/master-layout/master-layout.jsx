import './master-layout.css';
import React, { Component } from 'react';
import { browserHistory, Link } from 'react-router';
import Alert from 'react-s-alert';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
class MasterLayoutComponent extends Component {
  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props);
    this.state = {};
  }
  logOut = (e) => {
    Meteor.logout(error => {
      if (!error) {
        // console.log(Meteor.userId());
        browserHistory.push('/login')
      }
    });
  }
  componentWillMount() {
  }

  componentDidMount() { }

  componentWillUnmount() {
  }

  render() {
    return (<div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#">Find Jobs <span className="sr-only">(current)</span></a>
              </li>

              <li className="nav-item">
                <ul className="btn_list">
                  <li>
                    <div className="button_cstm"><a className="nav-link" href="#">Employees/Free Resume Post</a></div>
                  </li>

                  <li>
                    <div className="button_cstm"><a className="nav-link" href="#">Find Resumes</a></div>
                  </li>

                  <li>

                    <div className="row d-flex align-items-center p-3 my-3 text-white-50">
                      <button type="button" className="btn btn-primary button_cstm nav-link button_cstm" data-toggle="modal" data-target="#exampleModal">
                        Employers/Post Free Job
						</button>
                    </div>
                  </li>
                </ul>
              </li>

              {Meteor.userId() ?
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={this.logOut}>Logout</a>
                </li>
                :
                <>
                  <li className="nav-item">
                    <Link className="nav-link" to="/login">Signin</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/register">Register</Link>
                  </li>
                </>
              }

            </ul>
          </div>
        </div>
      </nav>
      <Alert stack={{ limit: 3 }} />
      {this.props.children}
    </div>);
  }
}

const MasterLayout = MasterLayoutComponent;
export { MasterLayout };
