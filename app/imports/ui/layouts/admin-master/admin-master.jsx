import './admin-master.css';
import React, { Component } from 'react';
import { AdminNavbar, AdminNavbarComponent } from '../../components/admin-navbar/admin-navbar';
import { AdminSidebar, AdminSidebarComponent } from '../../components/admin-sidebar/admin-sidebar';

class AdminMasterComponent extends Component {
  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
  }

  componentDidMount() { }

  componentWillUnmount() {
  }

  render() {
    return (<div className="container-fluid">
      <div className="row">
        <AdminSidebarComponent />
        <div className="page">
          <AdminNavbarComponent />
          {this.props.children}
        </div>
      </div>
    </div>);
  }
}

const AdminMaster = AdminMasterComponent;
export { AdminMaster };
