import './login.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

class LoginComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }
  loginUser = (e) =>{
    e.preventDefault();
    let email = e.target.email.value;
    let password = e.target.password.value;
    Meteor.loginWithPassword(email, password,error=>{
      if(error){
        console.log(error);
      }else{
        if(Meteor.user().roles[0] == 'admin'){
          this.props.router.push('/admin-dashboard');
        }
        if(Meteor.user().roles[0] == 'users'){
          this.props.router.push('/user-dashboard');
        }
        if(Meteor.user().roles[0] == 'pharmacy'){
          this.props.router.push('/pharmacy-dashboard');
        }
      }
    })
  }
  
  componentWillMount() { }

  componentDidMount() {
    if(Meteor.userId()){
      this.props.router.push('/');
    }
   }

  componentWillUnmount() { }

  render() {
    return (<div className="login">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4 offset-md-4">
            <div className="card">
              <div className="card-body">
                <form onSubmit={this.loginUser}>
                  <input type="email" name="email" className="email" />
                  <input type="password" name="password" className="email" />
                  <button type="submit">Login User</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}
const Login = withTracker(() => { return {}; })(LoginComponent);

export { Login, LoginComponent };
