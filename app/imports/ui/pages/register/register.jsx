import './register.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import router from 'react-router';
import CanadaProvinces from '../../../api/canada-provinces/canada-provinces-collection';
import CanadaCities from '../../../api/canada-cities/canada-cities-collection';
import Alert from 'react-s-alert';
class RegisterComponent extends Component {
  static propTypes = {};

  static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {};
  }
  registerUser = (e) => {
    e.preventDefault();
    let email = e.target.email.value;
    let password = e.target.password.value;

    let firstName = e.target.firstName.value;
    let lastName = e.target.lastName.value;
    let middleName = e.target.middleName.value;
    let phone = e.target.phone.value;
    let userProvinceId = e.target.userProvinceId.value;
    let userCityId = e.target.userCityId.value;
    let postalCode = e.target.postalCode.value;
    let userAddress = e.target.userAddress.value;
    let confirmPassword = e.target.confirmPassword.value;
    if (password != confirmPassword) {
      Alert.error('Password Mismatch!! Please try again !!')
    } else {
      Meteor.call('registerUser', email, password, error => {
        if (error) {
          console.log(error);
        } else {
          Meteor.loginWithPassword(email, password, error => {
            if (!error && Meteor.userId()) {
              Meteor.call('user-data.insert', {
                userId: Meteor.userId(),
                role: Meteor.user().roles[0],
                firstName,
                lastName,
                middleName,
                phone,
                userProvinceId,
                userCityId,
                postalCode, userAddress,
                profilePercentage : '25',

              }, error => {
                if (!error && Meteor.user().roles[0] == 'users') {
                  Alert.success('New User Created');
                  this.props.router.push('/user-dashboard');
                }else{
                  console.log(error);
                }
              })
            }
          })
        }
      });
    }
    // var id = Accounts.createUser({
    //   email: email,
    //   password: password,
    //   profile: { name: 'Vaibhav' }
    // }, error => {
    //   if (error) {
    //     console.log(error);
    //   }
    // });
  }
  componentWillMount() { }

  componentDidMount() {
    if (Meteor.userId()) {
      this.props.router.push('/');
    }
  }

  componentWillUnmount() { }

  render() {
    return (<div className="register">
      {/*  */}
      <section id="main_form">
        <form onSubmit={this.registerUser}>
          <div className="container">
            <div className="main_form_inner">
              <h3 align="center">USER REGISTRATION</h3>
              <div className="inner_main_form">
                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="firstName" placeholder="First Name" />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="lastName" placeholder="Last Name" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="middleName" placeholder="Middle Name (optional)" />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="email" placeholder="Enter Email" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="phone" placeholder="Phone" />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <select className="custom-select my-1 mr-sm-2" name="userProvinceId">
                            <option>Choose Province</option>
                            {this.props.provinces.map((pr) => (
                              <option key={pr._id} value={pr._id}>{pr.name}</option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <select className="custom-select my-1 mr-sm-2" name="userCityId">
                            <option>Choose City</option>
                            {this.props.cities.map(city => (
                              <option key={city._id} value={city._id}>{city.cityName}</option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="postalCode" placeholder="Postal Code" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="field">
                        <div className="form-group">
                          <textarea className="form-control icon_none" id="exampleTextarea" name="userAddress" placeholder="Address" rows="3"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="password" placeholder="Password" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="confirmPassword" placeholder="Confirm Password" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group btn_style_css">
                  <button type="submit" className="btn btn-default">Submit</button>
                </div>
              </div>

            </div>
          </div>
        </form>
      </section>
      {/*  */}
    </div>);
  }
}
const Register = withTracker(() => {
  const subs1 = Meteor.subscribe('canada-provinces.public');
  const subs2 = Meteor.subscribe('canada-cities.public');
  const ready = subs1.ready() && subs2.ready();
  const provinces = ready ? CanadaProvinces.find({}).fetch() : [];
  const cities = ready ? CanadaCities.find({}).fetch() : [];
  return {
    provinces, cities
  };
})(RegisterComponent);

export { Register, RegisterComponent };
