import './profile-completion.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

class ProfileCompletionComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<h2 className="profile-completion">Find me in app/imports/ui/pages/user/profile-completion/profile-completion</h2>);
  }
}
const ProfileCompletion = withTracker(() => { return {}; })(ProfileCompletionComponent);

export { ProfileCompletion, ProfileCompletionComponent };
