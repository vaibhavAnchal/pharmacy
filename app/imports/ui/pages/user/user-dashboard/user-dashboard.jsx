import './user-dashboard.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import UserData from '../../../../api/user-data/user-data-collection';
import { PersonalInformation, PersonalInformationComponent } from '../../../components/user/personal-information/personal-information';
import { InsuranceInformation, InsuranceInformationComponent } from '../../../components/user/insurance-information/insurance-information';
import { ShippingPreferences, ShippingPreferencesComponent } from '../../../components/user/shipping-preferences/shipping-preferences';
import Prescriptions from '../../../../api/prescriptions/prescriptions-collection';
import moment from 'moment';
class UserDashboardComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { provinces, cities, loading } = this.props;
    if (!Meteor.userId()) {
      this.props.router.push('/login');
    }

    // if(loading && provinces && cities){
    //   console.log(provinces, cities);
    // }
  }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="user-dashboard" id="bg_color">
      {this.props.userData.profilePercentage < 35 ?
        <PersonalInformationComponent ud={this.props.userData} />
        :
        this.props.userData.profilePercentage < 45 ?
          <InsuranceInformationComponent ud={this.props.userData} />
          :
          this.props.userData.profilePercentage < 55 ?
            <ShippingPreferencesComponent ud={this.props.userData} />
            :
            <section>
              <div id="brand_holder" className="form_sec_css">
                <div className="container">
                  <div id="form_holder" className="row">
                    <div className="col-lg-3">
                      <div className="logo">
                        <img src="images/logo.png" />
                      </div>
                    </div>

                    <div className="col-lg-9">
                      <div className="row">
                        <div className="col-lg-5 align-middle">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Job Type/Career</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Job Type/Career" />
                          </div>
                        </div>

                        <div className="col-lg-5 align-middle">
                          <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Where</label>
                            <input type="email" className="form-control border_chng_css" aria-describedby="emailHelp" placeholder="City,State,Zip Code" />
                          </div>
                        </div>

                        <div className="col-lg-2 align-middle">
                          <div className="form-group btn_margin">
                            <div className="button_cstm"><a className="nav-link" href="#">Find/Job</a></div>
                          </div>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>
              </div>

              {/* <!-- Main form --> */}

              <div id="inner_sec">
                <div className="container">

                  <div className="row">
                    <div className="col-lg-12">
                      <div className="listing_top_head">
                        <h1 align="center">My Prescriptions</h1>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-9">
                      {
                        this.props.myPrescriptions.map(mypre => (
                          <div key={mypre._id} className="bg_all_same description_page_css">
                            <div className="row">
                              <div className="col-lg-12">
                                <div className="inner_head_holder">
                                  <div className="row">
                                    <div className="col-lg-9">
                                      <ul className="addrs_csst">
                                        <li>
                                          <img src="images/calculator.png" /><span>PCM Canada</span>
                                        </li>

                                        <li>
                                          <img src="images/locate.png" /><span>Calgary, AB</span>
                                        </li>
                                      </ul>
                                    </div>
                                    <div className="col-lg-3">
                                      <div className="button_class_css">
                                        <a href="#">
                                          Quick Apply
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="row">
                                    <div className="col-lg-2">
                                      <div className="pay_css">
                                        <small className="font-weight-bold">
                                          Delivery required on or Before
                                         </small>
                                      </div>
                                    </div>

                                    <div className="col-lg-10">
                                      <div className="pay_css">
                                        <p className="text-info small font-weight-bold">
                                         21 Aug, 2018
                                        </p>
                                      </div>
                                    </div>

                                  </div>

                                </div>
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-lg-12">
                                <div className="inner_text_holder">
                                  <p className="bg-light p-1 my-1">
                                    <span className="text-danger"> Allergies Listed :</span> {mypre.prescriptionAllergy}
                                  </p>
                                  <p className="bg-light p-1 my-1">
                                    <span className="text-danger"> Medical Condition :</span> {mypre.prescriptionMedicalCondition}
                                  </p>
                                  <p className="bg-light p-1 my-1">
                                    <span className="text-danger"> Additional Notes :</span> {mypre.prescriptionNote}
                                  </p>
                                </div>

                                <div className="sme_btn_class button_class_css listing_btn">
                                  <a href="#">Posted on: <span className="text-warning">  {moment(mypre.created_at).format('ll')}</span></a>
                                </div>

                              </div>
                            </div>

                          </div>

                        ))
                      }
                    </div>

                    <div className="col-lg-3">
                      <div className="side_one bg_all_same inner_head_holder">
                        <h2>Get New Job Post Notification By Email</h2>

                        <div className="subscribe">
                          <form>
                            <div className="form-group">
                              <input type="email" className="form-control icon_none" aria-describedby="emailHelp" placeholder="Enter Your Email" />
                            </div>
                            <div className=" subscribe_btn sme_btn_class button_class_css">
                              <a href="#">
                                Submit
                    </a>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* <!-----------------------------------------------> */}

              {/* <!-- Social --> */}

            </section>
      }
      {/* <!-- Smart Wizard HTML --> */}
    </div>);
  }
}
const UserDashboard = withTracker(() => {
  const subs1 = Meteor.subscribe('user-data.private');
  const subs2 = Meteor.subscribe('prescriptions.private');
  const ready = subs1.ready() && subs2.ready();
  const loading = !ready;
  const userData = ready ? UserData.findOne(Meteor.userId) : {};
  const myPrescriptions = ready ? Prescriptions.find({}).fetch() : [];
  return { userData, loading, myPrescriptions };
})(UserDashboardComponent);

export { UserDashboard, UserDashboardComponent };
