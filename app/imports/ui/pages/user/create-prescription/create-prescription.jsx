import './create-prescription.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import UserData from '../../../../api/user-data/user-data-collection';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Alert from 'react-s-alert';
class CreatePrescriptionComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      deliverydate: moment(),
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(date) {
    this.setState({
      delivery: date,
    });
  }
  createPrescription = (e) => {
    e.preventDefault();
    let prescriptionDeliveryRequiredOn = this.state.delivery.format('lll');
    let prescriptionNote = e.target.prescriptionNote.value;
    let prescriptionAllergy = this.props.userData.allergy == 'yes' ?  e.target.prescriptionAllergy.value : '';
    let prescriptionMedicalCondition =  this.props.userData.medicalcondition == 'yes' ? e.target.prescriptionMedicalCondition.value : '';
    console.log(prescriptionDeliveryRequiredOn,prescriptionNote,prescriptionAllergy,prescriptionMedicalCondition);

    Meteor.call('prescriptions.insert',{
      userId : Meteor.userId(),
      prescriptionDeliveryRequiredOn,
      prescriptionNote,
      prescriptionAllergy,
      prescriptionMedicalCondition,
      created_at : new Date(),
    },error=> {
      if(error){
        console.log(error);
      }else{
        Alert.success("New Prescription Created");
        this.props.router.push('user-dashboard')
      }
    });
  }
  componentWillMount() {
    if (!Meteor.user() && !Meteor.userId()) {
      this.props.router.push('/login');
    }
  }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="create-prescription">

      <div id="submit_form">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="form_inner_sec bg_all_same">
                <h2 align="center">Prescription Details</h2>
                <form onSubmit={this.createPrescription}>
                  <div className="form-group">
                    <label>Delivery Required on Date</label>
                    <DatePicker
                      className="form-control icon_none"
                      selected={this.state.delivery}
                      dateFormat="DD-MM-YYYY"
                      onChange={this.handleChange}
                      defaultValue=''
                      placeholderText="DD-MM-YYYY"
                    />
                  </div>
                  <div className="form-group">
                    <textarea className="form-control icon_none" id="exampleFormControlTextarea1" name="prescriptionNote" placeholder="Additional Notes for Prescriptions" rows="3"></textarea>
                  </div>
                  {
                    this.props.userData.allergy == 'yes' ?
                      <div className="form-group">
                        <textarea className="form-control icon_none" id="exampleFormControlTextarea1" name="prescriptionAllergy" defaultValue="" placeholder="Please Explain your allergy types.." rows="3"></textarea>
                      </div>
                      :
                      ''
                  }
                  {
                    this.props.userData.medicalcondition == 'yes' ?
                      <div className="form-group">
                        <textarea className="form-control icon_none" id="exampleFormControlTextarea1" defaultValue="" name="prescriptionMedicalCondition" placeholder="Please Explain your Medical Condition" rows="3"></textarea>
                      </div>
                      :
                      'nothing'
                  }
                  <div className="form-group">
                    <label htmlFor="exampleFormControlFile1">File.docx Added</label>
                    <input type="file" className="form-control icon_none" id="file" name="pswd" />
                  </div>

                  <div className="form-group btn_css">
                    <button type="submit" className="btn btn-primary">Create Now</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}
const CreatePrescription = withTracker(() => {
  const subs1 = Meteor.subscribe('user-data.private');
  const ready = subs1.ready();
  const userData = ready ? UserData.findOne(Meteor.userId) : {};
  return { userData };
})(CreatePrescriptionComponent);

export { CreatePrescription, CreatePrescriptionComponent };
