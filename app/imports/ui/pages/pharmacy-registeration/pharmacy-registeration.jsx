import './pharmacy-registeration.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import CanadaProvinces from '../../../api/canada-provinces/canada-provinces-collection';
import CanadaCities from '../../../api/canada-cities/canada-cities-collection';
import Alert from 'react-s-alert';

class PharmacyRegisterationComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }
  registerPharmacy = (e) => {
    e.preventDefault();
    let email = e.target.email.value;
    let pharmacyPassword = e.target.pharmacyPassword.value;
    let confirmPassword = e.target.confirmPassword.value;
    let pharmacyName = e.target.pharmacyName.value;
    let contactPersonName = e.target.contactPersonName.value;
    let pharmacyPhone = e.target.pharmacyPhone.value;
    let contactPersonPhone = e.target.contactPersonPhone.value;
    let pharmacyProvinceId = e.target.pharmacyProvinceId.value;
    let pharmacyCityId = e.target.pharmacyCityId.value;
    let pharmacyPostalCode = e.target.pharmacyPostalCode.value;
    console.log(email, pharmacyPassword,confirmPassword, pharmacyName, contactPersonName, pharmacyPhone, contactPersonPhone, pharmacyProvinceId, pharmacyCityId, pharmacyPostalCode);
    if (pharmacyPassword != confirmPassword) {
      Alert.error('Password Mismatch!! Please try again !!')
    } else {
      Meteor.call('registerPharmacy', email, pharmacyPassword, error => {
        if (error) {
          console.log(error);
        } else {
          Meteor.loginWithPassword(email, pharmacyPassword, error => {
            if (!error && Meteor.userId()) {
              Meteor.call('user-data.insert', {
                userId: Meteor.userId(),
                role: Meteor.user().roles[0],
                pharmacyName,
                contactPersonName,
                pharmacyPhone,
                contactPersonPhone,
                pharmacyProvinceId,
                pharmacyCityId,
                pharmacyPostalCode,
                profilePercentage: '50',
              }, error => {
                if (!error && Meteor.user().roles[0] == 'pharmacy') {
                  Alert.success('New User Created');
                  this.props.router.push('/pharmacy-dashboard');
                } else {
                  console.log(error);
                }
              })
            }
          })
        }
      });
    }

  }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="register">
      {/*  */}
      <section id="main_form">
        <form onSubmit={this.registerPharmacy}>
          <div className="container">
            <div className="main_form_inner">
              <h3 align="center">PHARMACY REGISTRATION</h3>
              <div className="inner_main_form">
                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="pharmacyName" placeholder="Pharmacy Name" />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="contactPersonName" placeholder="Contact Person Name" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="pharmacyPhone" placeholder="Pharmacy Phone" />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="contactPersonPhone" placeholder="Contact Person Phone" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="email" placeholder="Enter Email" />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <select className="custom-select my-1 mr-sm-2" name="pharmacyProvinceId">
                            <option>Choose Province</option>
                            {this.props.provinces.map((pr) => (
                              <option key={pr._id} value={pr._id}>{pr.name}</option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <select className="custom-select my-1 mr-sm-2" name="pharmacyCityId">
                            <option>Choose City</option>
                            {this.props.cities.map(city => (
                              <option key={city._id} value={city._id}>{city.cityName}</option>
                            ))}
                          </select>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6">
                      <div className="field">
                        <div className="form-group">
                          <input type="text" className="form-control icon_none" name="pharmacyPostalCode" placeholder="Pharmacy Postal Code" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="field">
                        <div className="form-group">
                          <input type="password" className="form-control icon_none" name="pharmacyPassword" placeholder="Pharmacy Password" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="inner_holder">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="field">
                        <div className="form-group">
                          <input type="password" className="form-control icon_none" name="confirmPassword" placeholder="Confirm Password" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="form-group btn_style_css">
                  <button type="submit" className="btn btn-default">Submit</button>
                </div>
              </div>

            </div>
          </div>
        </form>
      </section>
      {/*  */}
    </div>);
  }
}
const PharmacyRegisteration = withTracker(() => {
  const subs1 = Meteor.subscribe('canada-provinces.public');
  const subs2 = Meteor.subscribe('canada-cities.public');
  const ready = subs1.ready() && subs2.ready();
  const provinces = ready ? CanadaProvinces.find({}).fetch() : [];
  const cities = ready ? CanadaCities.find({}).fetch() : [];
  return { provinces, cities };
})(PharmacyRegisterationComponent);

export { PharmacyRegisteration, PharmacyRegisterationComponent };
