import './order-board.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Prescriptions from '../../../api/prescriptions/prescriptions-collection';
import moment from 'moment';
import UserData from '../../../api/user-data/user-data-collection';
import CanadaCities from '../../../api/canada-cities/canada-cities-collection';
import CanadaProvinces from '../../../api/canada-provinces/canada-provinces-collection';
import {Link} from 'react-router';
class OrderBoardComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }
  getUserName(userId){
    const {allUsers} = this.props;
    const user = allUsers.filter(us => us.userId == userId)[0];
    if(user){
      return `${user.firstName} ${user.lastName}`;
    }
  }
  getUserLocation(userId){
    const {allUsers, provinces, cities} = this.props;
    const user = allUsers.filter(us => us.userId == userId)[0];
    if(user){
      const province = provinces.filter(pr => pr._id == user.userProvinceId)[0];
      const city = cities.filter(ct => ct._id == user.userCityId)[0];
      if(province && city){
        return `${province.name}, ${city.cityName}`
      }
    }
  }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="order-board">
      <section id="bg_color">


        <div id="brand_holder" className="form_sec_css">
          <div className="container">
            <div id="form_holder" className="row">
              <div className="col-lg-3">
                <div className="logo">
                  <img src="images/logo.png" />
                </div>
              </div>

              <div className="col-lg-9">
                <div className="row">
                  <div className="col-lg-5 align-middle">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">Job Type/Career</label>
                      <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Job Type/Career" />
                    </div>
                  </div>

                  <div className="col-lg-5 align-middle">
                    <div className="form-group">
                      <label htmlFor="exampleInputEmail1">Where</label>
                      <input type="email" className="form-control border_chng_css" aria-describedby="emailHelp" placeholder="City,State,Zip Code" />
                    </div>
                  </div>

                  <div className="col-lg-2 align-middle">
                    <div className="form-group btn_margin">
                      <div className="button_cstm"><a className="nav-link" href="#">Find/Job</a></div>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

        {/* <!-- Main form --> */}

        <div id="inner_sec">
          <div className="container">

            <div className="row">
              <div className="col-lg-12">
                {/* <div className="listing_top_head">
                  <h1 align="center">All Jobs / Web Designer Jobs <span>/ Web Designer Jobs in Calgary</span></h1>
                  <h3 align="center">41 + Web Designer Jobs in Calgary, AB</h3>
                </div> */}
              </div>
            </div>
            <div className="row">
              <div className="col-lg-9">
                {
                  this.props.allPrescriptions.map(mypre => (
                    <div key={mypre._id} className="bg_all_same description_page_css">
                      <div className="row">
                        <div className="col-lg-12">
                          <div className="inner_head_holder">
                            <div className="row">
                              <div className="col-lg-9">
                                <ul className="addrs_csst">
                                  <li>
                                    <img style={{'maxWidth':'10%'}} src="global/user.png" /><span className="small">Posted by : {this.getUserName(mypre.userId)}</span>
                                  </li>

                                  <li>
                                  <img style={{'maxWidth':'10%'}} src="global/location.png" /><span className="small">{this.getUserLocation(mypre.userId)}</span>
                                  </li>
                                </ul>
                              </div>
                              <div className="col-lg-3">
                                <div className="button_class_css">
                                  <Link to={`/full-prescription/${mypre._id}/${mypre.userId}`}>
                                    View Prescription
                                  </Link>
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-lg-2">
                                <div className="pay_css">
                                  <small className="font-weight-bold">
                                    Delivery required on or Before
                                         </small>
                                </div>
                              </div>

                              <div className="col-lg-10">
                                <div className="pay_css">
                                  <p className="text-info small font-weight-bold">
                                    21 Aug, 2018
                                        </p>
                                </div>
                              </div>

                            </div>

                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-lg-12">
                          <div className="inner_text_holder">
                            <p className="bg-light p-1 my-1">
                              <span className="text-danger"> Allergies Listed :</span> {mypre.prescriptionAllergy}
                            </p>
                            <p className="bg-light p-1 my-1">
                              <span className="text-danger"> Medical Condition :</span> {mypre.prescriptionMedicalCondition}
                            </p>
                            <p className="bg-light p-1 my-1">
                              <span className="text-danger"> Additional Notes :</span> {mypre.prescriptionNote}
                            </p>
                          </div>

                          <div className="sme_btn_class button_class_css listing_btn">
                            <a href="#">Posted on: <span className="text-warning">  {moment(mypre.created_at).format('ll')}</span></a>
                          </div>

                        </div>
                      </div>

                    </div>

                  ))
                }
              </div>

              <div className="col-lg-3">
                <div className="side_one bg_all_same inner_head_holder">
                  <h2>Get New Job Post Notification By Email</h2>

                  <div className="subscribe">
                    <form>
                      <div className="form-group">
                        <input type="email" className="form-control icon_none" aria-describedby="emailHelp" placeholder="Enter Your Email" />
                      </div>
                      <div className=" subscribe_btn sme_btn_class button_class_css">
                        <a href="#">
                          Submit
                </a>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* <!-----------------------------------------------> */}

        {/* <!-- Social --> */}

      </section>
    </div >);
  }
}
const OrderBoard = withTracker(() => {
  const subs1 = Meteor.subscribe('prescriptions.public');
  const subs2 = Meteor.subscribe('user-data.public');
  const subs3 = Meteor.subscribe('canada-provinces.public');
  const subs4 = Meteor.subscribe('canada-cities.public');
  const ready = subs1.ready() && subs2.ready() && subs3.ready() && subs4.ready();
  const allPrescriptions = ready ? Prescriptions.find({}, {sort: {created_at: -1}}).fetch() : [];
  const allUsers = ready ? UserData.find({}).fetch() : [];
  const cities = ready ? CanadaCities.find({}).fetch() : [];
  const provinces = ready ? CanadaProvinces.find({}).fetch() : [];

  return { allPrescriptions, allUsers, cities, provinces};
})(OrderBoardComponent);

export { OrderBoard, OrderBoardComponent };