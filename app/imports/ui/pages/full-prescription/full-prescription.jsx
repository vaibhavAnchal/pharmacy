import './full-prescription.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import UserData from '../../../api/user-data/user-data-collection';
import Prescriptions from '../../../api/prescriptions/prescriptions-collection';

class FullPrescriptionComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="full-prescription">
      <div id="inner_sec">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-9">
              <div className="bg_all_same description_page_css">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner_head_holder">
                      <div className="row">
                        <div className="col-lg-7">
                          <div className="heading">
                            <h2>Junior Graphic Designer/Marketing Cordinator</h2>
                            <small className="color_chng">Premium</small>
                          </div>
                        </div>

                        <div className="col-lg-3">
                          <div className="logo_here">
                            <img src="images/your logo.png" />
                          </div>
                        </div>

                        <div className="col-lg-2">
                          <div className="button_class_css">
                            <a href="#">
                              Full Time
					  							</a>
                          </div>
                        </div>


                      </div>

                      <ul className="addrs_csst">
                        <li>
                          <img src="images/calculator.png" /><span>PCM Canada</span>
                        </li>

                        <li>
                          <img src="images/locate.png" /><span>Calgary, AB</span>
                        </li>
                      </ul>


                      <div className="row">
                        <div className="col-lg-1">
                          <div className="pay_css">
                            <small>
                              Pay<br />
                              Type
					  							</small>
                          </div>
                        </div>

                        <div className="col-lg-11">
                          <div className="pay_css">
                            <small>
                              CA$ 18 Hourly<br />
                              Contractor
					  							</small>
                          </div>import moment from 'moment';
                        </div>

                      </div>

                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner_text_holder">
                      <h3>
                        The Standard Lorem Ipsum passage, used since the 1500s
									</h3>

                      <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>
                    </div>

                    <div className="inner_text_holder">
                      <h3>
                        Education:
									</h3>

                      <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
									</p>
                    </div>

                    <div className="inner_text_holder">
                      <h3>
                        Experience:
									</h3>

                      <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>

                    </div>

                    <div className="sme_btn_class button_class_css">
                      <a href="#">Apply Now</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>

            {/* SIDE SECTION */}
            <div className="col-lg-3">
              <div className="bg_all_same description_page_css">
                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner_head_holder">
                      <div className="row">
                        <div className="col-lg-9">
                          <div className="heading">
                            <h2>{this.props.userData.firstName} {this.props.userData.lastName}</h2>
                            <small className="color_chng">{this.props.userData.gender}</small>
                          </div>
                        </div>

                        <div className="col-lg-3">
                          <div className="logo_here">
                            <img src="/global/user.png" />
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-lg-1">
                          <div className="pay_css">
                            <small>
                              Pay<br />
                              Type
					  							</small>
                          </div>
                        </div>

                        <div className="col-lg-11">
                          <div className="pay_css">
                            <small>
                              CA$ 18 Hourly<br />
                              Contractor
					  							</small>
                          </div>
                        </div>

                      </div>

                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-12">
                    <div className="inner_text_holder">
                      <h3>
                        Shipping Address :
									    </h3>

                      <p>
                        {this.props.userData.userAddress}
                      </p>
                      {
                        this.props.otherShippingAddress ? 
                        'show address'
                        :
                        ''
                      }
                    </div>

                    <div className="inner_text_holder">
                      <h3>
                        Education:
									</h3>

                      <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
									</p>
                    </div>

                    <div className="inner_text_holder">
                      <h3>
                        Experience:
									</h3>

                      <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</p>

                    </div>

                    <div className="sme_btn_class button_class_css">
                      <a href="#">Apply Now</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>
            {/* SIDE SECTION */}
          </div>
        </div>
      </div>

      {this.props.params.prescriptionId}
    </div>);
  }
}
const FullPrescription = withTracker((props) => {
  const subs1 = Meteor.subscribe('user-data.public');
  const subs2 = Meteor.subscribe('prescriptions.public');
  const ready = subs1.ready() && subs2.ready();
  const prescriptionUserId = props.params.prescriptionUserId;
  const prescriptionId = props.params.prescriptionId;
  const prescriptions = ready ? Prescriptions.find({ _id: prescriptionId }).fetch()[0] : {};
  const userData = ready ? UserData.find({ userId: prescriptionUserId }).fetch()[0] : {};
  // const thisUserData = userData ? userData.filter(ud => ud.userId == prescriptionUserId)[0] : {};
  console.log(userData);
  return { userData, prescriptions };
})(FullPrescriptionComponent);

export { FullPrescription, FullPrescriptionComponent };
