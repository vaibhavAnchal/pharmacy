import './pharmacy-dashboard.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import UserData from '../../../../api/user-data/user-data-collection';
import CreateMembership from '../../../../api/create-membership/create-membership-collection';
import PharmacyMembership from '../../../../api/pharmacy-membership/pharmacy-membership-collection';
import { Alert } from 'reactstrap';
class PharmacyDashboardComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }
  pharmacyAdditionalFields = (e) => {
    e.preventDefault();
    let pharmacyIntroduction = e.target.pharmacyIntroduction.value;
    let membershipId = e.target.membershipId.value;

    Meteor.call('pharmacy-membership.insert', {
      userId: Meteor.userId(),
      membershipId,
      created_at: new Date(),
      paymentVerified: false,
    }, error => {
      if (error) {

      } else {
        Meteor.call('user-data.update', [this.props.userData._id, {
          pharmacyIntroduction,
          profilePercentage: '100',
        }], error => {
          if (!error) {
            Alert('Profile Complete !!');
          }
        });
      }
    });

  }
  membershipName(membershipId) {
    // const membershipName = CreateMembership.find({_id: membershipId});
    const membershipName = this.props.plans.filter(plan => plan._id === membershipId)[0];
    if(membershipName){
      return membershipName.membershipName;
    }
  }
  prescriptionsAllowed(membershipId){
    const prescriptionsAllowed = this.props.plans.filter(plan => plan._id === membershipId)[0];
    if(prescriptionsAllowed){
      return prescriptionsAllowed.prescriptionsAllowed;
    }
  }
  componentWillMount() { 
    if(!Meteor.userId()){
      this.props.router.push('/login');
    }
  }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="pharmacy-dashboard">

      {this.props.userData.profilePercentage == '50' ?
        <div className="container">
          <div id="smartwizard" className="container">
            <div id="step-2" className="">
              <form className="first_step_css" onSubmit={this.pharmacyAdditionalFields}>
                <h3>Complete Pharmacy Profile </h3>
                <div className="form-group">
                  <label className="gender_css" htmlFor="formGroupExampleInput">Verification Document 1</label>
                  <input type="file" name="pic" accept="image/*" />
                </div>
                <div className="form-group">
                  <label className="gender_css" htmlFor="formGroupExampleInput">Verification Document 2</label>
                  <input type="file" name="pic" accept="image/*" />
                </div>
                <div className="form-group">
                  <label htmlFor="formGroupExampleInput">Introduction about Pharmacy</label>
                  <textarea className="form-control icon_none" placeholder="Introduction" name="pharmacyIntroduction"></textarea>
                </div>
                <div className="form-group">
                  <select className="custom-select my-1 mr-sm-2" name="membershipId">
                    <option value="">Choose Membership</option>
                    {
                      this.props.plans.map(pl => (
                        <option key={pl._id} value={pl._id}>{pl.membershipName}</option>
                      ))
                    }
                  </select>
                </div>
                <div className="form-group btn_style_css">
                  <button type="submit" className="btn btn-default">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        :
        <div>
          <h1 className="mt-5 text-center">Your Current Membership is, {this.membershipName(this.props.activePlan.membershipId)} and you are allowed to click on <span className="text-danger">{this.prescriptionsAllowed(this.props.activePlan.membershipId)} </span> prescription(s) untill they are deliverd.</h1>
        </div>
      }

    </div>);
  }
}
const PharmacyDashboard = withTracker(() => {
  const subs1 = Meteor.subscribe('user-data.private');
  const subs2 = Meteor.subscribe('create-membership.public');
  const subs3 = Meteor.subscribe('pharmacy-membership.private');
  const ready = subs1.ready() && subs2.ready() && subs3.ready();
  const userData = ready ? UserData.findOne(Meteor.userId) : {};
  const activePlan = ready ? PharmacyMembership.findOne(Meteor.userId) : {};
  const plans = ready ? CreateMembership.find({}).fetch() : [];
  return { userData, plans, activePlan };
})(PharmacyDashboardComponent);

export { PharmacyDashboard, PharmacyDashboardComponent };
