import './admin-dashboard.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';

class AdminDashboardComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() { 
    if(!Meteor.userId() && !Meteor.user()){
      this.props.router.push('/login');
    }else{
        console.log(Meteor.user());
    }
  }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="admin-dashboard">
      <section className="dashboard-counts section-padding">
        <div className="container-fluid">
          <div className="row">
            {/* <!-- Count item widget--> */}
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-user"></i></div>
                <div className="name"><strong className="text-uppercase">New Clients</strong><span>Last 7 days</span>
                  <div className="count-number">25</div>
                </div>
              </div>
            </div>
            {/* <!-- Count item widget--> */}
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-padnote"></i></div>
                <div className="name"><strong className="text-uppercase">Work Orders</strong><span>Last 5 days</span>
                  <div className="count-number">400</div>
                </div>
              </div>
            </div>
            {/* <!-- Count item widget--> */}
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-check"></i></div>
                <div className="name"><strong className="text-uppercase">New Quotes</strong><span>Last 2 months</span>
                  <div className="count-number">342</div>
                </div>
              </div>
            </div>
            {/* <!-- Count item widget--> */}
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-bill"></i></div>
                <div className="name"><strong className="text-uppercase">New Invoices</strong><span>Last 2 days</span>
                  <div className="count-number">123</div>
                </div>
              </div>
            </div>
            {/* <!-- Count item widget--> */}
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-list"></i></div>
                <div className="name"><strong className="text-uppercase">Open Cases</strong><span>Last 3 months</span>
                  <div className="count-number">92</div>
                </div>
              </div>
            </div>
            {/* <!-- Count item widget--> */}
            <div className="col-xl-2 col-md-4 col-6">
              <div className="wrapper count-title d-flex">
                <div className="icon"><i className="icon-list-1"></i></div>
                <div className="name"><strong className="text-uppercase">New Cases</strong><span>Last 7 days</span>
                  <div className="count-number">70</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>);
  }
}
const AdminDashboard = withTracker(() => { return {}; })(AdminDashboardComponent);

export { AdminDashboard, AdminDashboardComponent };
