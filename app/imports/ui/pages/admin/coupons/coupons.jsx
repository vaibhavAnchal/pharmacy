import './coupons.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {Link} from 'react-router';

import 'react-datepicker/dist/react-datepicker.css';
import ManageCoupons from '../../../../api/manage-coupons/manage-coupons-collection';
class CouponsComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      startDateFrom: moment(),
      startDateTo: moment()
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeTo = this.handleChangeTo.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDateFrom: date,
    });
  }
  handleChangeTo(date) {
    this.setState({
      startDateTo: date
    });
  }

  addCoupon = (e) => {
    e.preventDefault();
    let couponName = e.target.coupon_name.value;
    let couponCode = e.target.coupon_code.value;
    let couponDiscount = e.target.coupon_discount.value;
    let validFrom = this.state.startDateFrom.format('LLLL');
    let validTo = this.state.startDateTo.format('LLLL');
    let couponDescription = e.target.coupon_description.value;
    console.log(couponCode, couponDescription, couponName, couponDiscount, validFrom, validTo);
    Meteor.call('manage-coupons.insert', {
      couponName,
      couponCode,
      couponDiscount,
      validFrom,
      validTo,
      couponDescription,
      createdAt: new Date()
    }, error => {
      if (!error) {
        $('.coup-form').slideToggle();
        $('input').value = '';
      } else {
        console.log(error);
      }
    });
  }
  componentWillMount() {

  }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="coupons">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 py-2">
            <button type="button" className="btn btn-primary" onClick={
              (e) => {
                e.preventDefault();
                $('.coup-form').slideToggle();
              }
            }>Create Coupon</button>
          </div>
        </div>
        <div className="row coup-form">
          <div className=" col-md-4 offset-md-4 mt-5">
            <div className="card">
              <div className="card-header d-flex align-items-center">
                <h4>Create Coupon</h4>
              </div>
              <div className="card-body">
                <form className="coupon-form" onSubmit={this.addCoupon}>
                  <div className="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Enter Coupon Name" name="coupon_name" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Coupon Code</label>
                    <input type="text" placeholder="Coupon code : diw45241" name="coupon_code" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Discount %</label>
                    <input type="number" placeholder="Enter Discount Percentage" name="coupon_discount" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Valid From</label>
                    <DatePicker
                      selected={this.state.startDateFrom}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Valid To</label>
                    <DatePicker
                      selected={this.state.startDateTo}
                      onChange={this.handleChangeTo}
                    />
                  </div>
                  <div className="form-group">
                    <label>Description</label>
                    <textarea className="form-control" name="coupon_description"></textarea>
                  </div>
                  <div className="form-group">
                    <input type="submit" value="Signin" className="btn btn-primary" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            {/* Show Coupons */}
            <div id="new-updates" className="card updates recent-updated">
              <div id="updates-header" className="card-header d-flex justify-content-between align-items-center">
                <h2 className="h5 display"><a data-toggle="collapse" data-parent="#new-updates" href="#updates-box" aria-expanded="true" aria-controls="updates-box">Listing All Coupons</a></h2><a data-toggle="collapse" data-parent="#new-updates" href="#updates-bo" aria-expanded="true" aria-controls="updates-box"><i className="fa fa-angle-down"></i></a>
              </div>
              <div id="updates-box" role="tabpanel" className="collapse show">
                <ul className="news list-unstyled">
                  {this.props.coupons.map((coupon, index) => (
                    <li key={index} className="d-flex justify-content-between">
                      <div className="left-col d-flex">
                        <div className="icon"><Link to={`/manage-coupon/${coupon._id}`}><i className="fa fa-pencil"></i></Link></div>
                        <div className="title"><strong>{coupon.couponName}</strong>
                          <p>{coupon.couponDescription}</p>
                          <div className="clearfix">
                          <small>{moment(coupon.validFrom).format('ll')}</small> TO <small>{moment(coupon.validTo).format('ll')}</small>
                        </div>
                        </div>
                      </div>
                      <div className="right-col text-right">
                        <div className="update-date">{coupon.couponDiscount} % </div>
                      </div>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
            {/* Show coupons ends */}
          </div>
        </div>
      </div>
    </div>);
  }
}
const Coupons = withTracker(() => {
  const subs1 = Meteor.subscribe('manage-coupons.public');
  const ready = subs1.ready();
  let coupons = ready ? ManageCoupons.find({}).fetch() : [];
  return { coupons };
})(CouponsComponent);

export { Coupons, CouponsComponent };
