import './manage-coupon.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import $ from 'jquery';
import ManageCoupons from '../../../../api/manage-coupons/manage-coupons-collection';
import { FormGroup, Label, Input } from 'reactstrap';

class ManageCouponComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      startDateFrom: null,
      startDateTo: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeTo = this.handleChangeTo.bind(this);
  }
  handleChange(date) {
    this.setState({
      startDateFrom: date,
    });
  }
  handleChangeTo(date) {
    this.setState({
      startDateTo: date
    });
  }

  updateCoupon = (e) => {
    e.preventDefault();
    let couponName = e.target.coupon_name.value;
    let couponCode = e.target.coupon_code.value;
    let couponDiscount = e.target.coupon_discount.value;
    let validFrom = this.state.startDateFrom == null ? this.props.coupon.validFrom : this.state.startDateFrom.format('LLLL');
    let validTo = this.state.startDateTo == null ? this.props.coupon.validTo : this.state.startDateTo.format('LLLL');
    let couponDescription = e.target.coupon_description.value;
    console.log(couponCode, couponDescription, couponName, couponDiscount, validFrom, validTo);
  }
  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillUnmount() { }

  render() {
    return (<div className="manage-coupon">

      <div className="container-fluid">
        <div className="row">
          <div className=" col-md-4 offset-md-4 mt-5">
            <div className="card">
              <div className="card-header d-flex align-items-center">
                <h4>Create Coupon</h4>
              </div>
              <div className="card-body">
                <form className="coupon-form" onSubmit={this.updateCoupon}>
                  <div className="form-group">
                    <label>Name</label>
                    <input type="text" placeholder="Enter Coupon Name" defaultValue={this.props.coupon.couponName} name="coupon_name" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Coupon Code</label>
                    <input type="text" placeholder="Coupon code : diw45241" defaultValue={this.props.coupon.couponCode} name="coupon_code" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Discount %</label>
                    <input type="number" placeholder="Enter Discount Percentage" defaultValue={this.props.coupon.couponDiscount} name="coupon_discount" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Valid From</label>
                    <DatePicker
                      className="form-control"
                      selected={this.state.startDateFrom}
                      onChange={this.handleChange}
                      dateFormat="DD-MM-YYYY"
                    />
                    {moment(this.props.coupon.validFrom).format('ll')}
                  </div>
                  <div className="form-group">
                    <label>Valid To</label>
                    <DatePicker
                      className="form-control w-100"
                      selected={this.state.startDateTo}
                      onChange={this.handleChangeTo}
                    />
                    {moment(this.props.coupon.validTo).format('ll')}
                  </div>
                  <FormGroup>
                    <Label for="exampleText">Coupon Description</Label>
                    <textarea name="coupon_description" className="form-control">{this.props.coupon.couponDescription}</textarea>

                  </FormGroup>
                  <div className="form-group">
                    <input type="submit" value="Signin" className="btn btn-primary" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>);
  }
}
const ManageCoupon = withTracker((props) => {
  let couponId = props.params.couponId;
  const subs1 = Meteor.subscribe('manage-coupons.public');
  const ready = subs1.ready();
  let coupon = ready ? ManageCoupons.findOne(couponId) : {};
  return { ready, coupon };
})(ManageCouponComponent);

export { ManageCoupon, ManageCouponComponent };
