import './memberships.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import $ from 'jquery';
import Alert from 'react-s-alert';
import CreateMembership from '../../../../api/create-membership/create-membership-collection';
class MembershipsComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }
  addMembershipPlan = (e) => {
    e.preventDefault()
    let membershipName = e.target.membership_name.value;
    let membershipCost = e.target.membership_cost.value;
    let prescriptionsAllowed = e.target.membership_prescriptions_allowed.value;
    Meteor.call('create-membership.insert', {
      membershipName,
      membershipCost,
      prescriptionsAllowed,
      created_at: new Date(),
    }, error => {
      if (!error) {
        Alert.success('New membership plan published !!');
        $('.coup-form').slideToggle();

      } else {
        console.log(error);
      }
    })
  }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div className="memberships">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12 py-2">
            <button type="button" className="btn btn-primary" onClick={
              (e) => {
                e.preventDefault();
                $('.coup-form').slideToggle();
              }
            }>Create Membership</button>
          </div>
        </div>
        <div className="row coup-form">
          <div className=" col-md-4 offset-md-4 mt-5">
            <div className="card">
              <div className="card-header d-flex align-items-center">
                <h4>Create Membership Plans</h4>
              </div>
              <div className="card-body">
                <form className="coupon-form" onSubmit={this.addMembershipPlan}>
                  <div className="form-group">
                    <label>Membership Name</label>
                    <input type="text" placeholder="Enter Member Name" name="membership_name" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Membership Cost</label>
                    <input type="text" placeholder="Membership Cost" name="membership_cost" className="form-control" />
                  </div>
                  <div className="form-group">
                    <label>Clicks Allowed on Prescription</label>
                    <input type="number" placeholder="How many prescriptions are allowed to be picked" name="membership_prescriptions_allowed" className="form-control" />
                  </div>
                  <div className="form-group">
                    <input type="submit" value="Create Now" className="btn btn-primary" />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>


      {/* List Memberships */}
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col">Membership Name</th>
                      <th scope="col">Price</th>
                      <th scope="col">Clicks Allowed</th>
                      <th scope="col">Handle</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.props.plans.map(pl => (
                        <tr key={pl._id}>
                          <th scope="row">{pl.membershipName}</th>
                          <td>$ {pl.membershipCost}</td>
                          <td>{pl.prescriptionsAllowed}</td>
                          <td>@mdo</td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* Listing Memberships */}
    </div>);
  }
}
const Memberships = withTracker(() => {
  const subs1 = Meteor.subscribe('create-membership.public');
  const ready = subs1.ready();
  const plans = ready ? CreateMembership.find({}).fetch() : [];
  return { plans };
})(MembershipsComponent);

export { Memberships, MembershipsComponent };
