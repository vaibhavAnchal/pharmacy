import './admin-sidebar.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router';

class AdminSidebarComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<nav className="side-navbar">
      <div className="side-navbar-wrapper">

        <div className="sidenav-header d-flex align-items-center justify-content-center">

          <div className="sidenav-header-inner text-center"><img src="img/avatar-7.jpg" alt="person" className="img-fluid rounded-circle" />
            <h2 className="h5">Nathan Andrews</h2><span>Web Developer</span>
          </div>

          <div className="sidenav-header-logo"><a href="index.html" className="brand-small text-center"> <strong>B</strong><strong className="text-primary">D</strong></a></div>
        </div>

        <div className="main-menu">
          <h5 className="sidenav-heading">Main</h5>
          <ul id="side-main-menu" className="side-menu list-unstyled">
            <li><Link to="/coupons"><i className="icon-home"></i>Coupons</Link></li>
            <li><a href="forms.html"> <i className="icon-form"></i>Forms                             </a></li>
            <li><a href="charts.html"> <i className="fa fa-bar-chart"></i>Charts                             </a></li>
            <li><a href="tables.html"> <i className="icon-grid"></i>Tables                             </a></li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i className="icon-interface-windows"></i>Example dropdown </a>
              <ul id="exampledropdownDropdown" className="collapse list-unstyled ">
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Page</a></li>
              </ul>
            </li>
            <li><a href="login.html"> <i className="icon-interface-windows"></i>Login page                             </a></li>
            <li> <a href="#"> <i className="icon-mail"></i>Demo
              <div className="badge badge-warning">6 New</div></a></li>
          </ul>
        </div>
        <div className="admin-menu">
          <h5 className="sidenav-heading">Second menu</h5>
          <ul id="side-admin-menu" className="side-menu list-unstyled">
            <li> <a href="#"> <i className="icon-screen"> </i>Demo</a></li>
            <li> <a href="#"> <i className="icon-flask"> </i>Demo
              <div className="badge badge-info">Special</div></a></li>
            <li> <a href=""> <i className="icon-flask"> </i>Demo</a></li>
            <li> <a href=""> <i className="icon-picture"> </i>Demo</a></li>
          </ul>
        </div>
      </div>
    </nav>);
  }
}
const AdminSidebar = withTracker(() => { return {}; })(AdminSidebarComponent);

export { AdminSidebar, AdminSidebarComponent };
