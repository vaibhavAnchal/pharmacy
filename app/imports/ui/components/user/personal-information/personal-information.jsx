import './personal-information.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { FormGroup, Label, Input } from 'reactstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Alert from 'react-s-alert';

class PersonalInformationComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      dob: moment(),
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(date) {
    this.setState({
      dob: date,
    });
  }
  personalInformation = (e) => {
    e.preventDefault();
    let dob = this.state.dob.format('LLLL');
    let gender = e.target.gender.value;
    let allergy = e.target.allergy.value;
    let medicalcondition = e.target.medicalcondition.value;
    let currency = e.target.currency.value;
    console.log(dob, gender, allergy, medicalcondition, currency);
    if ((dob && gender && allergy && medicalcondition && currency) == '') {
      Alert.error("Please add all fields");
    } else {
      Meteor.call('user-data.update', [this.props.ud._id, {
        dob,
        gender,
        allergy,
        medicalcondition,
        currency,
      }], error => {
        if (!error) {
          Meteor.call('user-data.update', [this.props.ud._id, {
            profilePercentage: '35',
          }], error => {
            if (!error) {
              Alert.success("Information Updated Successfully!!");
            }
          });
        } else {
          console.log(error);
        }
      });
    }

  }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (<div id="smartwizard" className="container bg-light">
      <div id="step-1" className="">
        <form className="first_step_css" onSubmit={this.personalInformation}>
          <h3>Personal Information</h3>
          <div className="form-group">
            <label className="gender_css" htmlFor="formGroupExampleInput">Profile Picture (optional)</label>
            <input type="file" name="pic" accept="image/*" />
          </div>
          <div className="form-group">
            <label>Date of Birth</label>
            <DatePicker
              className="form-control icon_none"
              selected={this.state.dob}
              dateFormat="DD-MM-YYYY"
              onChange={this.handleChange}
              defaultValue={this.state.dob}
            />
          </div>
          <FormGroup tag="fieldset">
            <Label for="exampleEmail2" sm={5}>Gender</Label>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="gender" defaultValue="male" />{' '}
                Male
            </Label>
              <Label check className="ml-5">
                <Input type="radio" name="gender" defaultValue="female" />{' '}
                Female
            </Label>
            </FormGroup>
          </FormGroup>
          <FormGroup tag="fieldset">
            <Label for="exampleEmail2" sm={5}>Do you have any Allergies ? </Label>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="allergy" defaultValue="yes" />{' '}
                Yes
            </Label>
              <Label check className="ml-5">
                <Input type="radio" name="allergy" defaultValue="no" />{' '}
                No
            </Label>
            </FormGroup>
          </FormGroup>
          <FormGroup tag="fieldset">
            <Label for="exampleEmail2" sm={5}>Do you have any Medical Condition ? </Label>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="medicalcondition" defaultValue="yes" />{' '}
                Yes
            </Label>
              <Label check className="ml-5">
                <Input type="radio" name="medicalcondition" defaultValue="no" />{' '}
                No
            </Label>
            </FormGroup>
          </FormGroup>
          <label className="my-1 mr-2 gender_css" htmlFor="inlineFormCustomSelectPref">Preferred Currency</label>
          <select className="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="currency">
            <option value="">Choose...</option>
            <option value="dollar">CAD ($)</option>
            <option value="pounds">POUNDS</option>
          </select>

          <div className="form-group btn_style_css">
            <button type="submit" className="btn btn-default">Next</button>
          </div>
        </form>
      </div>
    </div>);
  }
}
const PersonalInformation = withTracker(() => { return {}; })(PersonalInformationComponent);

export { PersonalInformation, PersonalInformationComponent };
