import './insurance-information.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import Alert from 'react-s-alert';

class InsuranceInformationComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }
  addInsurance = (e) => {
    e.preventDefault();
    let insuranceNumber = e.target.insurancenumber.value;
    Meteor.call('user-data.update', [this.props.ud._id, {
      insuranceNumber
    }], error => {
      if (!error) {
        Meteor.call('user-data.update', [this.props.ud._id, {
          profilePercentage: '45',
        }], error => {
          if (!error) {
            Alert.success("Information Updated Successfully!!");
          }
        });
      } else {
        console.log(error);
      }
    });
  }
  render() {
    return (
      <div id="smartwizard" className="container">
        <div id="step-2" className="">
          <form className="first_step_css" onSubmit={this.addInsurance}>
            <h3>Insurance Information</h3>
            <div className="form-group">
              <label htmlFor="formGroupExampleInput">Enter Insurance Number</label>
              <input type="text" className="form-control icon_none" placeholder="Manager, Employee" name="insurancenumber"/>
            </div>
            <div className="form-group btn_style_css">
              <button type="submit" className="btn btn-default">Submit</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
const InsuranceInformation = withTracker(() => { return {}; })(InsuranceInformationComponent);

export { InsuranceInformation, InsuranceInformationComponent };
