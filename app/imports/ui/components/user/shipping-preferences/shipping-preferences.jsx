import './shipping-preferences.css';
import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { FormGroup, Label, Input } from 'reactstrap';
import Alert from 'react-s-alert';
class ShippingPreferencesComponent extends Component {
  static propTypes = {};

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      shippreference: true,
    };
  }
  shipYes = (e) => {
    this.setState({
      shippreference: true,
    })
  }
  shipNo = (e) => {
    this.setState({
      shippreference: false,

    })
  }
  addShippingPreference = (e) => {
    e.preventDefault();
    shipToAnotherAddress = this.state.shippreference;
    address1 = e.target.addressone.value;
    address2 = e.target.addresstwo.value;
    Meteor.call('user-data.update', [this.props.ud._id, {
     otherShippingAddress : this.state.shippreference,
     address1,
     address2,
    }], error => {
      if (!error) {
        Meteor.call('user-data.update', [this.props.ud._id, {
          profilePercentage: '55',
        }], error => {
          if (!error) {
            Alert.success("Information Updated Successfully!!");
          }
        });
      } else {
        console.log(error);
      }
    });
  }
  noOtherAddress = (e) => {
    e.preventDefault();
    Meteor.call('user-data.update', [this.props.ud._id, {
      otherShippingAddress : this.state.shippreference,
     }], error => {
       if (!error) {
         Meteor.call('user-data.update', [this.props.ud._id, {
           profilePercentage: '55',
         }], error => {
           if (!error) {
             Alert.success("Information Updated Successfully!!");
           }
         });
       } else {
         console.log(error);
       }
     });
  }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  render() {
    return (
      <div id="smartwizard" className="container bg-light">
        <div id="step-3" className="">
          <form className="first_step_css" onSubmit={this.addShippingPreference}>
            <FormGroup tag="fieldset">
              <Label for="exampleEmail2" sm={12}>Do you want to add different  address for your prescriptions shipping ?</Label>
              <FormGroup check>
                <Label check>
                  <Input type="radio" name="ship_preference" defaultValue="yes" defaultChecked onClick={this.shipYes} />{' '}
                  Yes
            </Label>
                <Label check className="ml-5">
                  <Input type="radio" name="ship_preference" defaultValue="no" onClick={this.shipNo} />{' '}
                  No
            </Label>
              </FormGroup>
            </FormGroup>
            {
              this.state.shippreference ?
                <>
                  <div className="form-group">
                    <label className="gender_css" htmlFor="formGroupExampleInput2">Address 1</label>
                    <input type="text" className="form-control icon_none" name="addressone" placeholder="" />
                  </div>

                  <div className="form-group">
                    <label className="gender_css" htmlFor="formGroupExampleInput2">Address 2</label>
                    <input type="text" className="form-control icon_none" name="addresstwo" placeholder="" />
                  </div>

                  <div className="form-group btn_style_css">
                    <button type="submit" className="btn btn-default">Submit</button>
                  </div>
                </>
                :
                <div className="form-group btn_style_css">
                  <button type="button" className="btn btn-default" onClick={this.noOtherAddress}>Next</button>
                </div>
            }
          </form>
        </div>
      </div>
    );
  }
}
const ShippingPreferences = withTracker(() => {
  return { };
})(ShippingPreferencesComponent);

export { ShippingPreferences, ShippingPreferencesComponent };
