/**
 * Registration of the following files for the
 * CanadaProvinces API into the Server namespace.
 * ```
 * './api/canada-provinces/methods.js'
 * './api/canada-provinces/api.js'
 * './api/canada-provinces/fixtures.js'
 * './api/canada-provinces/publications.js'
 * ```
 * @namespace Server.CanadaProvinces
 */
import '../../api/canada-provinces/rpc-methods';
import '../../api/canada-provinces/fixtures';
import '../../api/canada-provinces/publications';
