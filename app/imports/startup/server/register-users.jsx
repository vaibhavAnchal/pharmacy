Meteor.startup(() => {
    if (Meteor.users.find().count() === 0) {
        const adminId = Accounts.createUser({
            email: 'vaibhav.anchal16@gmail.com',
            password: '123456'
        });
        Roles.addUsersToRoles(adminId, ['admin']);
    }
});

Meteor.methods({ 
    registerUser(email, password) {
        try {
            const userId = Accounts.createUser({
                email,
                password
            });
        Roles.addUsersToRoles(userId, ['users']);
        } catch (e) {
            throw(e);
            // return new Meteor.Error('Unable to create user');
        }
    },
    registerPharmacy(email, password) {
        try {
            const userId = Accounts.createUser({
                email,
                password
            });

        Roles.addUsersToRoles(userId, ['pharmacy']);
        } catch (e) {
            throw(e);
            // return new Meteor.Error('Unable to create user');
        }
    }
});