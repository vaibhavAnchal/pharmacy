/**
 * Registration of the following files for the
 * ManageCoupons API into the Server namespace.
 * ```
 * './api/manage-coupons/methods.js'
 * './api/manage-coupons/api.js'
 * './api/manage-coupons/fixtures.js'
 * './api/manage-coupons/publications.js'
 * ```
 * @namespace Server.ManageCoupons
 */
import '../../api/manage-coupons/rpc-methods';
import '../../api/manage-coupons/fixtures';
import '../../api/manage-coupons/publications';
