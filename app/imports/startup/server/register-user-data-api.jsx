/**
 * Registration of the following files for the
 * UserData API into the Server namespace.
 * ```
 * './api/user-data/methods.js'
 * './api/user-data/api.js'
 * './api/user-data/fixtures.js'
 * './api/user-data/publications.js'
 * ```
 * @namespace Server.UserData
 */
import '../../api/user-data/rpc-methods';
import '../../api/user-data/fixtures';
import '../../api/user-data/publications';
