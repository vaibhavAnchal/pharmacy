/**
 * Registration of the following files for the
 * CanadaCities API into the Server namespace.
 * ```
 * './api/canada-cities/methods.js'
 * './api/canada-cities/api.js'
 * './api/canada-cities/fixtures.js'
 * './api/canada-cities/publications.js'
 * ```
 * @namespace Server.CanadaCities
 */
import '../../api/canada-cities/rpc-methods';
import '../../api/canada-cities/fixtures';
import '../../api/canada-cities/publications';
