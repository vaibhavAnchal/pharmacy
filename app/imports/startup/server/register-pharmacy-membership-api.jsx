/**
 * Registration of the following files for the
 * PharmacyMembership API into the Server namespace.
 * ```
 * './api/pharmacy-membership/methods.js'
 * './api/pharmacy-membership/api.js'
 * './api/pharmacy-membership/fixtures.js'
 * './api/pharmacy-membership/publications.js'
 * ```
 * @namespace Server.PharmacyMembership
 */
import '../../api/pharmacy-membership/rpc-methods';
import '../../api/pharmacy-membership/fixtures';
import '../../api/pharmacy-membership/publications';
