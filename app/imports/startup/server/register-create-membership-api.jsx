/**
 * Registration of the following files for the
 * CreateMembership API into the Server namespace.
 * ```
 * './api/create-membership/methods.js'
 * './api/create-membership/api.js'
 * './api/create-membership/fixtures.js'
 * './api/create-membership/publications.js'
 * ```
 * @namespace Server.CreateMembership
 */
import '../../api/create-membership/rpc-methods';
import '../../api/create-membership/fixtures';
import '../../api/create-membership/publications';
