/**
 * Registration of the following files for the
 * Prescriptions API into the Server namespace.
 * ```
 * './api/prescriptions/methods.js'
 * './api/prescriptions/api.js'
 * './api/prescriptions/fixtures.js'
 * './api/prescriptions/publications.js'
 * ```
 * @namespace Server.Prescriptions
 */
import '../../api/prescriptions/rpc-methods';
import '../../api/prescriptions/fixtures';
import '../../api/prescriptions/publications';
