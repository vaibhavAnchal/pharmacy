
import './register-users';
import './register-manage-coupons-api.jsx';
import './register-canada-provinces-api.jsx';
import './register-canada-cities-api.jsx';
import './register-user-data-api.jsx';
import './register-pharmacy-membership-api.jsx';
import './register-create-membership-api.jsx';
import './register-prescriptions-api.jsx';