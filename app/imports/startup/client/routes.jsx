import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

/**
 * The React Router client side routing definitions.
 * @namespace Client.Routes
 * @desc This is the main definition for the react router.
 */

import * as Component from './templates.jsx';

const Routes = () => (
  <Router history={browserHistory}>

    <Route path="/" component={Component.MasterLayout}>
      <IndexRoute component={Component.Home} />
    </Route>

    <Route path="/register" component={Component.MasterLayout}>
      <IndexRoute component={Component.Register} />
    </Route>

    <Route path="/login" component={Component.MasterLayout}>
      <IndexRoute component={Component.Login} />
    </Route>

    <Route path="/admin-dashboard" component={Component.AdminMaster}>
      <IndexRoute component={Component.AdminDashboard} />
    </Route>

    <Route path="/coupons" component={Component.AdminMaster}>
      <IndexRoute component={Component.Coupons} />
    </Route>

    <Route path="/manage-coupon/:couponId" component={Component.AdminMaster}>
      <IndexRoute component={Component.ManageCoupon} />
    </Route>

    <Route path="/user-dashboard" component={Component.MasterLayout}>
      <IndexRoute component={Component.UserDashboard} />
    </Route>

    <Route path="/profile-completion" component={Component.MasterLayout}>
      <IndexRoute component={Component.ProfileCompletion} />
    </Route>

    <Route path="/pharmacy-registeration" component={Component.MasterLayout}>
      <IndexRoute component={Component.PharmacyRegisteration} />
    </Route>

    <Route path="/pharmacy-dashboard" component={Component.MasterLayout}>
      <IndexRoute component={Component.PharmacyDashboard} />
    </Route>

    <Route path="/memberships" component={Component.AdminMaster}>
      <IndexRoute component={Component.Memberships} />
    </Route>

    <Route path="/order-board" component={Component.MasterLayout}>
      <IndexRoute component={Component.OrderBoard} />
    </Route>

    <Route path="/create-prescription" component={Component.MasterLayout}>
      <IndexRoute component={Component.CreatePrescription} />
    </Route>

    <Route path="/full-prescription/:prescriptionId/:prescriptionUserId" component={Component.MasterLayout}>
      <IndexRoute component={Component.FullPrescription} />
    </Route>
    <Route path="*" component={Component.MasterLayout}>
      <IndexRoute component={Component.NotFound} />
    </Route>
  </Router>
);

export default Routes;
