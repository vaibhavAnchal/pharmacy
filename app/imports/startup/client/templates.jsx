import { FullPrescription } from '/imports/ui/pages/full-prescription/full-prescription.jsx';

import { CreatePrescription } from '/imports/ui/pages/user/create-prescription/create-prescription.jsx';

import { OrderBoard } from '/imports/ui/pages/order-board/order-board.jsx';

import { Memberships } from '/imports/ui/pages/admin/memberships/memberships.jsx';

import { PharmacyDashboard } from '/imports/ui/pages/pharmacy/pharmacy-dashboard/pharmacy-dashboard.jsx';

import { PharmacyRegisteration } from '/imports/ui/pages/pharmacy-registeration/pharmacy-registeration.jsx';

import { ShippingPreferences } from '/imports/ui/components/user/shipping-preferences/shipping-preferences.jsx';

import { InsuranceInformation } from '/imports/ui/components/user/insurance-information/insurance-information.jsx';

import { PersonalInformation } from '/imports/ui/components/user/personal-information/personal-information.jsx';

import { ProfileCompletion } from '/imports/ui/pages/user/profile-completion/profile-completion.jsx';

import { UserDashboard } from '/imports/ui/pages/user/user-dashboard/user-dashboard.jsx';

import { ManageCoupon } from '/imports/ui/pages/admin/manage-coupon/manage-coupon.jsx';


import { Coupons } from '/imports/ui/pages/admin/coupons/coupons.jsx';

import { AdminNavbar } from '/imports/ui/components/admin-navbar/admin-navbar.jsx';

import { AdminMaster } from '/imports/ui/layouts/admin-master/admin-master.jsx';

import { AdminSidebar } from '/imports/ui/components/admin-sidebar/admin-sidebar.jsx';

import { AdminDashboard } from '/imports/ui/pages/admin/admin-dashboard/admin-dashboard.jsx';

import { Login } from '/imports/ui/pages/login/login.jsx';

import { Register } from '/imports/ui/pages/register/register.jsx';

import { NotFound } from '/imports/ui/pages/not-found/not-found.jsx';

import { Home } from '/imports/ui/pages/home/home.jsx';

import { MasterLayout } from '/imports/ui/layouts/master-layout/master-layout.jsx';

/**
 *  React Templates
 *  * Layouts: Top level UI concept, pages are rendered within.
 *  * Pages: Main content UI concept, contains page context and UI components.
 *  * Components: Modular UI concept.
 *
 * @namespace Client.Templates
 * @memberof Client
 */

export { MasterLayout };

export { Home };

export { NotFound };

export { Register };

export { Login };

export { AdminDashboard };

export { AdminSidebar };

export { AdminMaster };

export { AdminNavbar };

export { Coupons };

export { ManageCoupon };

export { UserDashboard };

export { ProfileCompletion };

export { PersonalInformation };

export { InsuranceInformation };

export { ShippingPreferences };

export { PharmacyRegisteration };

export { PharmacyDashboard };

export { Memberships };

export { OrderBoard };

export { CreatePrescription };

export { FullPrescription };
