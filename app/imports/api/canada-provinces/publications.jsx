import { Meteor } from 'meteor/meteor';
import CanadaProvinces from './canada-provinces-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.CanadaProvinces
 * @member Publications
 */
Meteor.publish('canada-provinces.public', function canadaProvincesPublic() {
  const cursor = CanadaProvinces.find({
    // userId: { $exists: false },
  }, {
    fields: CanadaProvinces.publicFields,
  });

  return cursor;
});

Meteor.publish('canada-provinces.private', function canadaProvincesPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = CanadaProvinces.find({
    userId: this.userId,
  }, {
    fields: CanadaProvinces.privateFields,
  });

  return cursor;
});
