import { Meteor } from 'meteor/meteor';
import CanadaProvinces from './canada-provinces-collection';
/**
 * If the CanadaProvinces collection is empty on server start, and you'd like to
 * populate it with some data here is a handy spot.
 *
 * Example:
 * ```
 *  import Trucks from './trucks.jsx'
 *  if (CanadaProvinces.find().count() === 0) {
 *      const data = JSON.parse(Assets.getText('canada-provinces.json')) || [ {} ];
 *      data.forEach((datum) => {
 *          CanadaProvinces.insert(datum);
 *      });
 *  }
 *
 * ```
 * @memberof Server.CanadaProvinces
 * @member Fixtures
 */
Meteor.startup(() => {
    if (CanadaProvinces.find({}).count() === 0) {
        const provinces = [
            {
                "name": "Alberta",
                "abbreviation": "AB"
            },
            {
                "name": "British Columbia",
                "abbreviation": "BC"
            },
            {
                "name": "Manitoba",
                "abbreviation": "MB"
            },
            {
                "name": "New Brunswick",
                "abbreviation": "NB"
            },
            {
                "name": "Newfoundland and Labrador",
                "abbreviation": "NL"
            },
            {
                "name": "Northwest Territories",
                "abbreviation": "NT"
            },
            {
                "name": "Nova Scotia",
                "abbreviation": "NS"
            },
            {
                "name": "Nunavut",
                "abbreviation": "NU"
            },
            {
                "name": "Ontario",
                "abbreviation": "ON"
            },
            {
                "name": "Prince Edward Island",
                "abbreviation": "PE"
            },
            {
                "name": "Quebec",
                "abbreviation": "QC"
            },
            {
                "name": "Saskatchewan",
                "abbreviation": "SK"
            },
            {
                "name": "Yukon Territory",
                "abbreviation": "YT"
            }
        ];

        provinces.forEach((province) => {
                     CanadaProvinces.insert({
                         name : province.name,
                         abbreviation: province.abbreviation
                     });
            });

        
    }
});
