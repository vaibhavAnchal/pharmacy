import { Meteor } from 'meteor/meteor';
import ManageCoupons from './manage-coupons-collection';

/**
 * If the ManageCoupons collection is empty on server start, and you'd like to
 * populate it with some data here is a handy spot.
 *
 * Example:
 * ```
 *  import Trucks from './trucks.jsx'
 *  if (ManageCoupons.find().count() === 0) {
 *      const data = JSON.parse(Assets.getText('manage-coupons.json')) || [ {} ];
 *      data.forEach((datum) => {
 *          ManageCoupons.insert(datum);
 *      });
 *  }
 *
 * ```
 * @memberof Server.ManageCoupons
 * @member Fixtures
 */
Meteor.startup(() => {

});
