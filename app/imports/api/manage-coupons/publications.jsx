import { Meteor } from 'meteor/meteor';
import ManageCoupons from './manage-coupons-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.ManageCoupons
 * @member Publications
 */
Meteor.publish('manage-coupons.public', function manageCouponsPublic() {
  const cursor = ManageCoupons.find({
    userId: { $exists: false },
  }, {
    fields: ManageCoupons.publicFields,
  });

  return cursor;
});

Meteor.publish('manage-coupons.private', function manageCouponsPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = ManageCoupons.find({
    userId: this.userId,
  }, {
    fields: ManageCoupons.privateFields,
  });

  return cursor;
});
