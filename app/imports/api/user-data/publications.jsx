import { Meteor } from 'meteor/meteor';
import UserData from './user-data-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.UserData
 * @member Publications
 */
Meteor.publish('user-data.public', function userDataPublic() {
  const cursor = UserData.find({
    // userId: { $exists: false },
  }, {
    fields: UserData.publicFields,
  });

  return cursor;
});

Meteor.publish('user-data.private', function userDataPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = UserData.find({
    userId: this.userId,
  }, {
    fields: UserData.privateFields,
  });

  return cursor;
});
