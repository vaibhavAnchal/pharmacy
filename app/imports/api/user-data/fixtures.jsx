import { Meteor } from 'meteor/meteor';
import UserData from './user-data-collection';

/**
 * If the UserData collection is empty on server start, and you'd like to
 * populate it with some data here is a handy spot.
 *
 * Example:
 * ```
 *  import Trucks from './trucks.jsx'
 *  if (UserData.find().count() === 0) {
 *      const data = JSON.parse(Assets.getText('user-data.json')) || [ {} ];
 *      data.forEach((datum) => {
 *          UserData.insert(datum);
 *      });
 *  }
 *
 * ```
 * @memberof Server.UserData
 * @member Fixtures
 */
Meteor.startup(() => {

});
