import { Meteor } from 'meteor/meteor';
import CreateMembership from './create-membership-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.CreateMembership
 * @member Publications
 */
Meteor.publish('create-membership.public', function createMembershipPublic() {
  const cursor = CreateMembership.find({
    userId: { $exists: false },
  }, {
    fields: CreateMembership.publicFields,
  });

  return cursor;
});

Meteor.publish('create-membership.private', function createMembershipPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = CreateMembership.find({
    userId: this.userId,
  }, {
    fields: CreateMembership.privateFields,
  });

  return cursor;
});
