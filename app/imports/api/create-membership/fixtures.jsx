import { Meteor } from 'meteor/meteor';
import CreateMembership from './create-membership-collection';

/**
 * If the CreateMembership collection is empty on server start, and you'd like to
 * populate it with some data here is a handy spot.
 *
 * Example:
 * ```
 *  import Trucks from './trucks.jsx'
 *  if (CreateMembership.find().count() === 0) {
 *      const data = JSON.parse(Assets.getText('create-membership.json')) || [ {} ];
 *      data.forEach((datum) => {
 *          CreateMembership.insert(datum);
 *      });
 *  }
 *
 * ```
 * @memberof Server.CreateMembership
 * @member Fixtures
 */
Meteor.startup(() => {

});
