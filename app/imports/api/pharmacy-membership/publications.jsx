import { Meteor } from 'meteor/meteor';
import PharmacyMembership from './pharmacy-membership-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.PharmacyMembership
 * @member Publications
 */
Meteor.publish('pharmacy-membership.public', function pharmacyMembershipPublic() {
  const cursor = PharmacyMembership.find({
    userId: { $exists: false },
  }, {
    fields: PharmacyMembership.publicFields,
  });

  return cursor;
});

Meteor.publish('pharmacy-membership.private', function pharmacyMembershipPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = PharmacyMembership.find({
    userId: this.userId,
  }, {
    fields: PharmacyMembership.privateFields,
  });

  return cursor;
});
