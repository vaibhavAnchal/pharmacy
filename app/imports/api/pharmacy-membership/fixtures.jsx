import { Meteor } from 'meteor/meteor';
import PharmacyMembership from './pharmacy-membership-collection';

/**
 * If the PharmacyMembership collection is empty on server start, and you'd like to
 * populate it with some data here is a handy spot.
 *
 * Example:
 * ```
 *  import Trucks from './trucks.jsx'
 *  if (PharmacyMembership.find().count() === 0) {
 *      const data = JSON.parse(Assets.getText('pharmacy-membership.json')) || [ {} ];
 *      data.forEach((datum) => {
 *          PharmacyMembership.insert(datum);
 *      });
 *  }
 *
 * ```
 * @memberof Server.PharmacyMembership
 * @member Fixtures
 */
Meteor.startup(() => {

});
