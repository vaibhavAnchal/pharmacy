import { Meteor } from 'meteor/meteor';
import CanadaCities from './canada-cities-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.CanadaCities
 * @member Publications
 */
Meteor.publish('canada-cities.public', function canadaCitiesPublic() {
  const cursor = CanadaCities.find({
    // userId: { $exists: false },
  }, {
    fields: CanadaCities.publicFields,
  });

  return cursor;
});

Meteor.publish('canada-cities.private', function canadaCitiesPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = CanadaCities.find({
    userId: this.userId,
  }, {
    fields: CanadaCities.privateFields,
  });

  return cursor;
});
