import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';
import { _ } from 'meteor/underscore';

import CanadaCities from './canada-cities-collection.jsx';

/**
 * Using ValidatedMethod (maintained by MDG) is the best
 * practice by Meteor Development Group (MDG),
 * and as such is used here to validate that method
 * calls from the client are properly handled.
 *
 */

/**
 * Client side insert method.
 * *Disabled by default. Uncomment return of method to enable*
 *
 * @memberof Server.CanadaCities
 * @method
 * @property { string }     name        String that defines the method.
 * @property { function }   validate    Is run before the main execution.
 * @property { function }   run         The main action that is executed.
 */
const insertCanadaCities = new ValidatedMethod({
  name: 'canada-cities.insert',
  validate: null,
  run(doc) {
    return CanadaCities.insert(doc);
  },
});

/**
 * Client side update method.
 * *Disabled by default. Uncomment return of method to enable*
 *
 * @memberof Server.CanadaCities
 * @method
 * @property { string }     name        String that defines the method.
 * @property { function }   validate    Is run before the main execution.
 * @property { function }   run         The main action that is executed.
 */
const updateCanadaCities = new ValidatedMethod({
  name: 'canada-cities.update',
  validate: null,
  run([docId, obj]) {
    return CanadaCities.update(docId, { $set: obj });
  },
});

/**
 * Client side remove method.
 * *Disabled by default. Uncomment return of method to enable*
 *
 * @memberof Server.CanadaCities
 * @method
 * @property { string }     name        String that defines the method.
 * @property { function }   validate    Is run before the main execution.
 * @property { function }   run         The main action that is executed.
 */
const removeCanadaCities = new ValidatedMethod({
  name: 'canada-cities.remove',
  validate: null,
  run(docId) {
    return CanadaCities.remove(docId);
  },
});

const RATE_LIMITED_METHODS = _.pluck([
  insertCanadaCities, updateCanadaCities, removeCanadaCities,
], 'name');

if (Meteor.isServer) {
  const OPERATIONS = 5;
  const PER_SECOND = 1 * 1000; // milliseconds
  // Only allow 5 list operations per connection per second.
  DDPRateLimiter.addRule({
    name(name) {
      return _.contains(RATE_LIMITED_METHODS, name);
    },

    // Rate limit per connection ID.
    connectionId() { return true; },
  }, OPERATIONS, PER_SECOND);
}
