import { Meteor } from 'meteor/meteor';
import Prescriptions from './prescriptions-collection';

/**
 * If the Prescriptions collection is empty on server start, and you'd like to
 * populate it with some data here is a handy spot.
 *
 * Example:
 * ```
 *  import Trucks from './trucks.jsx'
 *  if (Prescriptions.find().count() === 0) {
 *      const data = JSON.parse(Assets.getText('prescriptions.json')) || [ {} ];
 *      data.forEach((datum) => {
 *          Prescriptions.insert(datum);
 *      });
 *  }
 *
 * ```
 * @memberof Server.Prescriptions
 * @member Fixtures
 */
Meteor.startup(() => {

});
