import { Meteor } from 'meteor/meteor';
import Prescriptions from './prescriptions-collection';

/**
 * Collection publications to the client.  Publications must
 * return a cursor object.
 *
 * @memberof Server.Prescriptions
 * @member Publications
 */
Meteor.publish('prescriptions.public', function prescriptionsPublic() {
  const cursor = Prescriptions.find({
    // userId: { $exists: false },
  }, {
    fields: Prescriptions.publicFields,
  });
  return cursor;
});

Meteor.publish('prescriptions.private', function prescriptionsPrivate() {
  if (!this.userId) {
    return this.ready();
  }

  const cursor = Prescriptions.find({
    userId: this.userId,
  }, {
    fields: Prescriptions.privateFields,
  });

  return cursor;
});
